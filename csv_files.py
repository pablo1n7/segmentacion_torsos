from sklearn import decomposition
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def csv(file, file_out, ids):
    procrustes = np.load(file)
    pcs = 60
    pca = decomposition.PCA(n_components=pcs, svd_solver='randomized')
    pca.fit(procrustes.reshape(113, -1))
    print("Con n_components = {} el valor es: {} ".format(pcs, pca.explained_variance_ratio_.sum()))
    x_out = pca.transform(procrustes.reshape(113, -1))
    df = pd.DataFrame()
    df["id"] = ids
    for i in np.arange(pcs):
        df["pc-{}".format(i+1)] = x_out[:, i]
    #df["altura"] = altura
    #df["peso"] = peso
    #df["sexo"] = sexo
    #bmis = np.array(list(map( lambda pa: pa[0] / (pa[1]*pa[1])  ,zip(peso,altura))))
    #df["bmi"] = bmis
    #df["icc"] = icc
    #df["ict"] = ict
    df.to_csv(file_out, sep=";")
    
def main():
    ids = np.load("procrustes_id_r_0.npy")
    #     data = pd.read_excel("./planilla_colecta.xlsx")
    #     data = data[data['id'].isin(ids)]
    #     sexo = np.array(list(map(lambda x: np.array(data[data['id'] == int(x)][["sexo"]])[0], ids))).reshape(113)
    #     sexo["f" == sexo] = "red"
    #     sexo["m" == sexo] = "blue"
    #     sexo["m " == sexo] = "blue"
    #     sexo = np.array(list(map(lambda x: np.array(data[data['id'] == int(x)][["sexo"]])[0], ids))).reshape(113)
    #     peso = np.array(list(map(lambda x: np.array(data[data['id'] == int(x)][["peso"]])[0], ids))).reshape(113)
    #     altura = np.array(list(map(lambda x: np.array(data[data['id'] == int(x)][["promedio altura"]])[0] / 100 , ids))).reshape(113)
    #     balance = np.array(list(map(lambda x: np.array(data[data['id'] == int(x)][["Body fat range"]])[0] , ids)), dtype=np.str).reshape(113)
    #     cad = np.array(list(map(lambda x: np.array(data[data['id'] == int(x)][["promedio cir cad"]])[0], ids))).reshape(113)
    #     cin = np.array(list(map(lambda x: np.array(data[data['id'] == int(x)][["promedio cir cin"]])[0], ids))).reshape(113)
    #     ict = cin / altura
    #     icc = cin / cad
    #     bmis = np.array(list(map( lambda pa: bmi_cal(pa[0] / (pa[1]*pa[1]))  ,zip(peso,altura))))
    #     balance_c = np.array(list(map( lambda pa: balance_to_colors(pa),balance)))
    #     sexo["f" == sexo] = "red"
    #     sexo["m" == sexo] = "blue"
    #     sexo["m " == sexo] = "blue"
    for i in range(25):
        csv("procrustes_r_{}.npy".format(i), "data_r_{}.csv".format(i), ids )


if __name__ == "__main__":
        main()
